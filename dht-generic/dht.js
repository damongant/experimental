var crypto = require('crypto')
  , bigint = require('bigint')

var BUCKET_BITS = 8
var BUCKET_NUM = 2
var ID_SIZE = 160

var DHT = function() {
	this.nodes = []
}
DHT.addNode = function(id, data) {
	var node = {
		id: id,
		data: data
	}
	this.nodes.push(node)
}

var DHTNode = function(id, data) {
	if (id instanceof bigint)
		this.id = id;
	else if (id instanceof Buffer)
		this.id = bigint.fromBuffer(id)
	else
		throw new Error("id format unknown")
	this.isBad = false
	this.data = data;
	this.lastUpdate = (new Date()).getTime() / 1000
}
DHTNode.prototype.distanceTo = function(otherNode) {
	return this.id.xor(otherNode.id)
}
DHTNode.prototype.partition = function() {
	return this.id
		.shiftRight(ID_SIZE - BUCKET_BITS)
		.mul(BUCKET_NUM)
		.div(Math.pow(2, BUCKET_BITS))
		.toNumber();
}
DHTNode.prototype.markGood = function() {
	this.lastUpdate = Date.now()
}
DHTNode.prototype.markBad = function() {
	this.isBad = true
}
DHTNode.prototype.isGood = function() {
	var lastUpdateUnix = this.lastUpdate
	var nowUnix = new Date().getTime() / 1000
	return (nowUnix - lastUpdateUnix < 900 && !this.isBad) // 15 minutes
}

var id = crypto.createHash('sha1').update("foobar").digest();
var node = new DHTNode(id, null)
console.log(node.partition())