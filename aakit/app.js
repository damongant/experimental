var express = require('express');
var http = require('http');
var path = require('path');
var socketio = require('socket.io');

var app = express();
var server = http.createServer(app)
var io = socketio.listen(server);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.render('app')
});

server.listen(app.get('port'), "0.0.0.0", function(){
  console.log('Express server listening on port ' + app.get('port'));
});

io.on('connection', function(socket) {
	socket.emit('init', {motd: "Welcome to AAKit 0.0.1 Beta"});
	socket.on('getManifest', function(data) {
		socket.emit('manifest', {
			images: [{
				src: "/res/background-mainmenu.png",
				name: "background-mainmenu"
			},
			{
				src: "/res/textbox.png",
				name: "textbox"
			},
			{
				src: "/res/textbox-label.png",
				name: "textbox-label"
			},
			{
				src: "/res/textbox-nolabel.png",
				name: "textbox-nolabel"
			},
			{
				src: "/res/judge/judge-idle-1.gif",
				name: "judge-idle-1"
			},
			{
				src: "/res/judge/judge-idle-2.gif",
				name: "judge-idle-2"
			},
			{
				src: "/res/judge/judge-idle-3.gif",
				name: "judge-idle-3"
			},
			{
				src: "/res/background/judgestand.png",
				name: "background-judgestand"
			}],
			audios: [{
				src: "/res/sfx-bip.wav",
				name: "sfx-bip"
			}],
			animations: [{
				name: 'judge-idle',
				frames: [{
					name: 'judge-idle-1',
					duration: 4000
				},
				{
					name: 'judge-idle-2',
					duration: 60
				},
				{
					name: 'judge-idle-3',
					duration: 60
				},
				{
					name: 'judge-idle-2',
					duration: 60
				}]
			}]
		})
	})
	socket.on('next', function(data) {
		socket.emit('text', {text: "Nice typewriter effect with beeping and automatic breaks. Isn't that nice?"})
	})
})