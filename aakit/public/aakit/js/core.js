function AAKit(io, loaded) {
	var self = this
	var loaded = loaded
	var socket = {}
	var topScreen
	var touchScreen
	var images = {}
	var audios = {}
	var animations = {}
	var gameElements = []

	numImagesLoaded = 0

	this.init = function(cb) {
		topScreen = document.getElementById('topScreen')
    	touchScreen = document.getElementById('touchScreen')
		console.log("Connecting to IO...");
		socket = io.connect("http://" + document.location.host + "/");
	  	socket.on('init', function (data) {
	  		console.log("IO connected, loading resources");
			socket.on('manifest', function(data) {
				if (data.images)
					$.each(data.images, loadImage)
				if (data.audios)
					$.each(data.audios, loadAudio)
				if (data.animations)
					$.each(data.animations, loadAnimation)
				console.log("Starting render loop")
				self.renderLoop()
				if (cb)
					cb()
			})
			socket.emit('getManifest')
	  	});
	}

	this.renderLoop = function() {
		function step(timeStamp) {
			clearScreens()
			$.each(gameElements, function(offset, gameElement) {
				gameElement.step(timeStamp);
			})
			$.each(gameElements, function(offset, gameElement) {
				gameElement.render();
			})
			window.requestAnimationFrame(step);
		}
		window.requestAnimationFrame(step);
	}

	this.getAudio = function(name) {
		return audios[name]
	}

	this.getImage = function(name) {
		return images[name]
	}

	this.getAnimation = function(name) {
		return animations[name]
	}

	this.getTopScreen = function() {
		return topScreen;
	}

	this.getTouchScreen = function() {
		return touchScreen;
	}

	this.addGameElement = function(element) {
		gameElements.push(element)
		element.init()
	}

	this.removeGameElement = function(element) {
		var index = gameElements.indexOf(element)
		if (index != -1) {
			gameElements.splice(index, 1);
			element.bye()
		}
		else
			console.log("Warning: removeGameElement failed")
	}

	function initCanvas() {
		topScreen = document.getElementById('topScreen')
    	touchScreen = document.getElementById('touchScreen')
	}

	function clearScreens() {
		topScreen.getContext('2d').clearRect(0,0,256,192)
	}

	function loadImage(index, value) {
		console.log("loading image " + value.name)
		images[value.name] = new Image();
		images[value.name].src = value.src;
	}

	function loadAudio(index, value) {
		console.log('loading audio ' + value.name)
		audios[value.name] = new Audio(value.src)
	}

	function loadAnimation(index, value) {
		console.log('loading animation ' + value.name)
		animations[value.name] = value.frames
	}
}
