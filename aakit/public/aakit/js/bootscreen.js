function BootScreen(engine) {
	var gt = 0
	var gt_offset = 0

	var handlers = []

	var backgroundMainmenu

	var top
	var touch
	var opacity = 0

	this.on = function(event, callback) {
		if (!handlers[event])
			handlers[event] = []
		handlers[event].push(callback)
	}

	this.emit = function(event, data) {
		if (!handlers[event])
			return
		$.each(handlers[event], function(index, handler) {
			handler(data)
		})
	}

	this.init = function() {
		gt_offset = window.mozAnimationStartTime
		top = engine.getTopScreen()
		touch = engine.getTouchScreen()
		backgroundMainmenu = engine.getImage('background-mainmenu')
	}

	this.step = function(gametime) {
		gt = gametime - gt_offset;
		opacity = gt * 0.0015
	}

	this.render = function() {
		var ctx = top.getContext('2d')
		ctx.moveTo(60,60)
		ctx.lineTo(30,30)
		ctx.lineTo(65,60)
		ctx.strokeStyle = "#000 2px";
		ctx.stroke();
		var ctx2 = touch.getContext('2d');
		ctx2.drawImage(backgroundMainmenu, 0, 0)
	}

	this.bye = function() {

	}
}