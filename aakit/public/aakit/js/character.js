function Character(engine, sAnimation) {
	var gt = 0
	var gt_offset = 0

	var handlers = []
	
	var top = {}

	this.animation = "judge-idle"
	this.loop = true
	this.background = 'background-judgestand'

	this.on = function(event, callback) {
		if (!handlers[event])
			handlers[event] = []
		handlers[event].push(callback)
	}

	this.emit = function(event, data) {
		if (!handlers[event])
			return
		$.each(handlers[event], function(index, handler) {
			handler(data)
		})
	}

	this.init = function() {
		gt_offset = window.animationStartTime
		top = engine.getTopScreen()
	}

	this.step = function(gametime) {
		if (!gt_offset)
			gt_offset = gametime //we can't have nice things
		gt = gametime - gt_offset;
		opacity = gt * 0.0015
	}

	this.render = function() {
		var ctx = top.getContext('2d')
		var image = getFrame(engine.getAnimation(this.animation), gt)
		ctx.drawImage(engine.getImage(this.background), 0,0)
		ctx.drawImage(image, 0,0)
	}

	this.bye = function() {

	}

	function getFrame(frames, gt) {
		duration = 0
		for (i in frames) {
			frames[i].startat = duration
			duration += frames[i].duration
			frames[i].stopat = duration
		}

		var time = gt % duration
		var retval
		for (i in frames) {
			if (frames[i].startat <= time && frames[i].stopat > time) {
				retval = frames[i]
			}
		}
		return engine.getImage(retval.name)
	}
}