function MainMenu(engine) {
	var gt = 0
	var gt_offset = 0

	var handlers = []

	var backgroundMainmenu

	var canvas

	this.on = function(event, callback) {
		if (!handlers[event])
			handlers[event] = []
		handlers[event].push(callback)
	}

	this.emit = function(event, data) {
		if (!handlers[event])
			return
		$.each(handlers[event], function(index, handler) {
			handler(data)
		})
	}

	this.init = function() {
		gt_offset = window.mozAnimationStartTime
		canvas = engine.getTouchScreen()
		backgroundMainmenu = engine.getImage('background-mainmenu')
	}

	this.step = function(gametime) {
		gt = gametime - gt_offset;
	}

	this.render = function() {
		var ctx = canvas.getContext('2d')
		ctx.drawImage(backgroundMainmenu, 0, 0)
		var ctx2 = engine.getTopScreen().getContext('2d');
		ctx2.drawImage(backgroundMainmenu, 0, 0)
	}

	this.bye = function() {

	}
}