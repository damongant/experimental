function TextBox(engine) {
	this.text = ""
	
	//canvas
	var canvas = engine.getTopScreen()
	
	//gametime
	var gt = 0;
	var gt_offset = 0;

	//Drawing line
	var line = 0;

	//Misc
	var done = false;
	var handlers = []

	//audio bleep states
	var sNumWords = 0;
	var sWordDensity = 0;

	//audio
	var bleep;
	var textbox;
	var textboxNoLabel
	var textboxLabel

	this.on = function(event, callback) {
		if (!handlers[event])
			handlers[event] = []
		handlers[event].push(callback)
	}

	this.emit = function(event, data) {
		if (!handlers[event])
			return
		$.each(handlers[event], function(index, handler) {
			handler(data)
		})
	}

	this.init = function() {
		//gt_offset = window.mozAnimationStartTime
		bleep = engine.getAudio('sfx-bip')
		bleep.preservesPitch = false
		bleep.playbackRate.value = 2;
		textbox = engine.getImage('textbox')
		textboxLabel = engine.getImage('textbox-label')
		textboxNoLabel = engine.getImage('textbox-nolabel')
		bleep.loop = 1;
	}

	this.step = function(gametime) {
		if (!gt_offset)
			gt_offset = gt
		gt = gametime - gt_offset;

		//todo: beep sound
		//console.log(engine)
		//engine.play('sfx-bip')
	}

	this.render = function() {
		var lText

		//Shoten Text to animate animating
		if (this.text.length > gt/70)
			lText = this.text.substr(0, gt/70)
		else {
			lText = this.text;
			if (!done) {
				this.emit('animationEnd');
				done = !done
			}
		}

		//Reset line when rendering new frame
		line = 0

		generateBeep(lText, this.text);

		//Off with the text to the drawing board
		drawText(lText, gt)
	}

	this.bye = function() {
		bleep.pause()
		console.log("TextBox says goodbye <3")
	}

	function generateBeep(lText, text) {
		if (lText == text) {
			bleep.pause()
			return
		}

		var current = text.substr(lText.length, 1)
		if (!(/^[a-z0-9]+$/i.test(current)) || current == "") {
			bleep.pause()
		}
		else {
			bleep.play()
			//bleep.playbackRate = 1.1; // Breaks the playback

		}
	}

	function generateBeepLegacy(lText) {
		//Todo: playback rate
		var numWords = lText.split(" ").length
		//sNumWords = last function calls number of words

		//check if there's a new word
		if (numWords > sNumWords) {
			engine.play('sfx-bip')
			sNumWords = numWords;

			//new word -> reset density
			sWordDensity = 0
		}

		var lastWord = lText.split(" ").splice(lText.length-1)
		var wordDensity = lastWord.length % 4
		if (wordDensity > sWordDensity) {
			engine.play('sfx-bip')
			sWordDensity = wordDensity
		}
	}

	function drawText(text, frame) {
		//Sanity check
		if (!text || text.length < 1)
			return;

		//Get Context
		var ctx = canvas.getContext('2d');
		ctx.font = "7px 'PWExtended'";
		ctx.fillStyle = "#FFF"
		ctx.textBaseline = "center"
		ctx.textAlign = "center"
		if (line == 0) {
			ctx.globalAlpha = 0.5
			ctx.drawImage(textbox, 0, 128)
			if (false)
				ctx.drawImage(textboxNoLabel, 0, 128)
			else {
				ctx.drawImage(textboxLabel, 0, 119)
				ctx.globalAlpha = 1
				ctx.fillText("Phoenix", 24, 123)
			}
			ctx.globalAlpha = 1
		}

		ctx.font = "10px 'PWExtended'";
		ctx.textBaseline = "top"
		ctx.textAlign = "left"

		var maxLength = 256 - 10 - 10;

		if (ctx.measureText(text).width > maxLength) {
			var words = text.split(" ");
			var totalLength = words.length;
			var i = totalLength;
			var subset = text.split(" ", i).join()
			do {
				i--
				var subset = text.split(" ", i).join(" ")
			} while (ctx.measureText(subset).width > maxLength)
			ctx.fillText(subset, 10, 138 + (16*line))
			var remainingtext = words.slice(i).join(" ")
			line++;
			drawText(remainingtext, frame)
		}
		else {
			ctx.fillText(text, 10, 138 + (16*line))
		}
	}
}